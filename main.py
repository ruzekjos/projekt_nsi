from flask import Flask, json, request, render_template
from routes import register_routes
import os
import secrets
import hashlib
import serial
import datetime
import sqlite3
import threading
import time
import paho.mqtt.client as mqtt
from serial_manager import init_serial, ser, lock, is_serial_initialized

# Create Flask app instance
app = Flask(__name__)
register_routes(app)
app.secret_key = secrets.token_hex(16)

# Database path
database = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'database.db')

lock = threading.Lock()


last_serial_record = None
last_serial_record_updated = threading.Condition()

last_mqtt_record = None

# MQTT configuration
MQTT_BROKER = 'test.mosquitto.org'
MQTT_PORT = 1883  # You can also use 8883 for SSL
MQTT_TOPIC = 'sensor/data'

# MQTT callback
def on_message(client, userdata, message):
    global last_mqtt_record
    print("Received payload:", message.payload.decode())
    data = json.loads(message.payload.decode())
    if 'timestamp' in data and 'temperature' in data:
        timestamp = data['timestamp']
        temperature = data['temperature']
        conn = sqlite3.connect(database)
        cursor = conn.cursor()
        cursor.execute('INSERT INTO data (timestamp, temperature) VALUES (?, ?)', (timestamp, temperature))
        conn.commit()
        conn.close()
        conn = sqlite3.connect(database)
        cursor = conn.cursor()
        cursor.execute("UPDATE last_values SET last_mqtt_record = ?", (timestamp,))
        conn.commit()
        conn.close()

client = mqtt.Client()
client.on_message = on_message
client.connect(MQTT_BROKER)
client.subscribe(MQTT_TOPIC, qos=0)
client.loop_start()

# Function to read from serial port
def read_serial():
    global ser, last_serial_record
    print("ser in read serial", ser)
    while True:
        if ser and ser.in_waiting > 0:
            print("Reading from serial port")
            with lock:
                try:
                    line = ser.readline().decode('utf-8').rstrip()
                    data = json.loads(line)  # Parse the JSON string
                    temperature = float(data['temperature'])  # Extract the temperature
                    timestamp = data['timestamp']  # Extract the timestamp

                    with last_serial_record_updated:
                        last_serial_record = timestamp
                        # Notify any waiting threads that last_serial_record has been updated
                        last_serial_record_updated.notify_all()
                    print(f"Timestamp: {timestamp}, Temperature: {temperature}")
                    # Store the received value into the 'data' table
                    conn = sqlite3.connect(database)
                    cursor = conn.cursor()
                    cursor.execute("INSERT INTO data (temperature, timestamp) VALUES (?, ?)", (temperature, timestamp))
                    conn.commit()
                    conn.close()

                    conn = sqlite3.connect(database)
                    cursor = conn.cursor()
                    cursor.execute("UPDATE last_values SET last_serial_record = ?", (timestamp,))
                    conn.commit()
                    conn.close()

                except Exception as e:
                    print(f"Error reading serial data: {line}")

                    print(f"Error reading serial data: {e}")


# Function to write to serial port
def write_serial(timeInSeconds, isMQTT):
    global ser
    print("write_serial called")  # Debugging print statement
    print(f"ser: {ser}")  # Print the value of ser

    with lock:
        if ser:
            print("ser is not None")  # Debugging print statement
            try:
                message = f"{timeInSeconds},{isMQTT}\n"  #thonny is waiting if \n not included !!!!
                ser.write(message.encode())
                print(f"Sent to serial port: {message}")
            except Exception as e:
                print(f"Error writing to serial port: {e}")
# Ensure the database and tables exist
def init_db():
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS data (
            id INTEGER PRIMARY KEY,
            timestamp TEXT NOT NULL,
            temperature REAL NOT NULL
        )
    ''')
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            username TEXT NOT NULL,
            password TEXT NOT NULL
        )
    ''')
    conn.commit()
    conn.close()

# Initialize the database
init_db()

read_serial_thread = None

def start_read_serial_thread():
    global read_serial_thread
    read_serial_thread = threading.Thread(target=read_serial)
    read_serial_thread.daemon = True
    read_serial_thread.start()

def create_app():
    global ser, read_serial_thread
    ser = init_serial()

    while not is_serial_initialized():
        time.sleep(1)

    start_read_serial_thread()
    
    return app

if __name__ == '__main__':
    try:
        app = create_app()
        app.run(host='0.0.0.0', port=3000, debug=True, use_reloader=False)
    except Exception as e:
        print(f"Failed to start the application: {e}")

