import network
import time
import ubinascii
import random
import json
from umqtt.simple import MQTTClient
from machine import Pin, ADC
import utime
import sys
import select

# Replace with your Wi-Fi credentials and MQTT broker details
SSID = 'Zyxel_E341_EXT'
PASSWORD = 'H78LCU7A7BK7D478'
MQTT_BROKER = 'test.mosquitto.org'
MQTT_TOPIC = 'sensor/data'
PORT = 1883

led = Pin('LED', Pin.OUT)

# Simulate temperature readings
def simul_temp():
    return random.uniform(15, 25)

# Connect to Wi-Fi
def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(SSID, PASSWORD)
    while not wlan.isconnected():
        print('Connecting to network...')  # UART communication
        time.sleep(1)
    print('Connected to', SSID)  # UART communication
    print('IP Address:', wlan.ifconfig()[0])  # UART communication

sleep_time = 10
MQTT = False

def read_serial():
    global MQTT
    global sleep_time  # Use the global variable
    if select.select([sys.stdin], [], [], 1)[0]:  # Check for available data with a timeout of 1 second
        line = sys.stdin.readline().strip()
        #print('Received from serial port:', line)
        if line:
            #print('Received from serial port:', line)
            try:
                timeInSeconds, isMQTT = line.split(',')
                #print('cas:', timeInSeconds)
                #print('mqtt:', isMQTT)
                    
                # Convert timeInSeconds to an integer and isMQTT to a boolean
                sleep_time = int(timeInSeconds)
                MQTT = isMQTT.lower() == 'true'
                led.value(1)
                time.sleep(2)
                led.value(0)
                #print('sleep_time:', sleep_time)
                #print('MQTT:', MQTT)
                    
            except ValueError:
                print('Unknown values:', line)
                
# Publish data to MQTT
def publish_data():

        global sleep_time 
        global MQTT 
        #client_id = ubinascii.hexlify(network.WLAN().config('mac'), ':').decode()
        client = MQTTClient(client_id="RaspberryPiPico", server=MQTT_BROKER, port=PORT)
        #client.connect()
        print('Connected to MQTT Broker')  # UART communication

        while True:
            
            read_serial()

            temperature = simul_temp()
            #print('after read serial')  # UART communication

            # Blink LED to indicate activity
            led.value(1)
            time.sleep(0.5)
            led.value(0)
            
            timestamp = utime.localtime()
            formatted_timestamp = "{:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}".format(timestamp[0], timestamp[1], timestamp[2], timestamp[3], timestamp[4], timestamp[5])

            data = {
                "timestamp": formatted_timestamp,
                "temperature": temperature
            }
            message = json.dumps(data)  # Convert the dictionary to a JSON string

            if MQTT:
                client.publish(MQTT_TOPIC, message)
                #print(f'Published message [{MQTT_TOPIC}]: {message}') # UART communication

            elif not MQTT:
                print("{}".format(message))
            
            time.sleep(sleep_time)

# Main execution
#connect_to_wifi()
publish_data()
