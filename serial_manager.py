import threading
import time
import serial

# Shared state
lock = threading.Lock()
ser = None

def init_serial(max_retries=10, retry_interval=1):
    global ser
    retries = 0
    while ser is None and retries < max_retries:
        try:
            ser = serial.Serial('COM7', 9600)
            ser.flushInput()
            print(f"ser initialized: {ser}")  # Debugging print statement
            return ser

        except serial.SerialException as e:
            retries += 1
            print(f"Unable to open COM port: {e}. Retrying in {retry_interval} second(s)... ({retries}/{max_retries})")
            time.sleep(retry_interval)
    
    if ser is None:
        raise Exception("Failed to initialize the serial port after multiple attempts.")
    
def is_serial_initialized():
    global ser
    print("check")  # Debugging print statement
    return ser is not None

