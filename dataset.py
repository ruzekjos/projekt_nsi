import sqlite3, hashlib
from datetime import datetime, timedelta
import random
import os
 
numRecords = 10
dbFile = os.path.join(os.getcwd(), 'database.db')
tableName = 'data'
minTemp = 20.0
maxTemp = 30.0
startDate = '2024-03-17 00:00:00'
period = 300


conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS {tableName} (
    id INTEGER PRIMARY KEY,
    timestamp TEXT NOT NULL,
    temperature REAL NOT NULL
);
"""
cursor.execute(createQuery)

countQuery = f"SELECT COUNT(*) FROM {tableName}"
cursor.execute(countQuery)
count = cursor.fetchone()[0]

if count == 0:
    startTime = datetime.strptime(startDate, '%Y-%m-%d %H:%M:%S')
else:
    maxTimestampQuery = f"SELECT MAX(timestamp) FROM {tableName}"
    cursor.execute(maxTimestampQuery)
    maxTimestamp = cursor.fetchone()[0]
    maxTimestampDatetime = datetime.strptime(maxTimestamp, '%Y-%m-%d %H:%M:%S')
    startTime = maxTimestampDatetime + timedelta(seconds=period)

for i in range(numRecords):
    timestamp = startTime + timedelta(seconds=i*period)
    temperature = random.uniform(minTemp, maxTemp)
    insertQuery = f"INSERT INTO {tableName} (timestamp, temperature) VALUES (?, ?)"
    cursor.execute(insertQuery, (timestamp.strftime('%Y-%m-%d %H:%M:%S'), temperature))

conn.commit()
conn.close()

tableName = 'users'

conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS {tableName} (
    id INTEGER PRIMARY KEY,
    username TEXT NOT NULL,
    password TEXT NOT NULL
);
"""
cursor.execute(createQuery)
conn.commit()
conn.close()


tableName = 'user_status'

conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS user_status
    (logged_in BOOLEAN, 
    current_user TEXT)
"""
cursor.execute(createQuery)

cursor.execute("INSERT INTO user_status (logged_in, current_user) VALUES (?, ?)", (False, "No user"))

conn.commit()
conn.close()

tableName = 'last_values'

conn = sqlite3.connect(dbFile)
cursor = conn.cursor()

createQuery = f"""
CREATE TABLE IF NOT EXISTS last_values
    (last_serial_record TEXT, 
    last_mqtt_record TEXT)
"""
cursor.execute(createQuery)

cursor.execute("INSERT INTO last_values (last_serial_record, last_mqtt_record) VALUES (?, ?)", ("Unknown", "Unknown"))

conn.commit()
conn.close()