from flask import Blueprint, jsonify, json, request, flash, session
from datetime import datetime
import sqlite3, hashlib
from serial_manager import ser


api_routes_bp = Blueprint('api_routes', __name__)

@api_routes_bp.route('/api/measurments', methods=['GET'])
def get_values():
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data")
    measurmets = cursor.fetchall()
    conn.close()
    for i in range(len(measurmets)):
        measurmets[i] = {"id": measurmets[i][0], "timestamp": measurmets[i][1], "temperature": measurmets[i][2]}
    return jsonify(measurmets), 200

@api_routes_bp.route('/api/measurments/<int:measurment_id>', methods=['GET'])
def get_single_measurment(measurment_id):
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data WHERE id=?", (measurment_id,))
    measurment = cursor.fetchone()
    conn.close()    
    if measurment:
        return jsonify(measurment), 200
    else:
        return jsonify({"message": "Record not found"}), 404

@api_routes_bp.route('/api/measurments', methods=['POST'])
def create_measurment():
    from main import database
    new_value = request.get_json()
    flash(new_value)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("INSERT INTO data (timestamp, temperature) VALUES (?, ?)", (new_value['timestamp'], new_value['temperature']))
    conn.commit()
    conn.close()    
    return jsonify({'timestamp':new_value['timestamp'], 'temperature':new_value['temperature']}), 201

@api_routes_bp.route('/api/measurments/<int:number_of_deletions>', methods=['DELETE'])
def delete_record(number_of_deletions):
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT COUNT(*) FROM data")
    total_rows = cursor.fetchone()[0]

    if number_of_deletions > 0 and number_of_deletions <= total_rows:
        cursor.execute("DELETE FROM data WHERE id IN (SELECT id FROM data ORDER BY id ASC LIMIT ?)", (number_of_deletions,))
        cursor.execute("UPDATE data SET id = id - ?", (number_of_deletions,))
    else:
        cursor.execute("DELETE FROM data")

    conn.commit()
    conn.close()
    return jsonify({"message": f"Deleted {number_of_deletions} value(s)"}), 200

@api_routes_bp.route('/api/users', methods=['GET'])
def get_creds():
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users")
    users = cursor.fetchall()
    conn.close()
    for i in range(len(users)):
        users[i] = {"id": users[i][0], "username": users[i][1], "password": users[i][2]}
    return jsonify(users), 200

@api_routes_bp.route('/api/users/<int:user_id>', methods=['GET'])
def get_cred(user_id):
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE id=?", (user_id,))
    user = cursor.fetchone()
    conn.close()    
    if user:
        return jsonify({"id": user[0], "timestamp": user[1], "temperature": user[2]}), 200
    else:
        return jsonify({"message": "Credentials not found"}), 404

@api_routes_bp.route('/api/users', methods=['POST'])
def add_user():
    from main import database
    new_user = request.get_json()
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE username=?", (new_user['username'],))
    existing_user = cursor.fetchone()
    if existing_user:
        conn.close()
        flash('Username already exists. Please choose another one.', 'error')
        return jsonify({"error": "Username already exists"}), 400
    else:
        new_user['password'] = hashlib.sha256(new_user['password'].encode()).hexdigest()
        cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (new_user['username'], new_user['password']))
        cursor.execute("UPDATE user_status SET logged_in = ?, current_user = ?", (True, new_user['username']))
        conn.commit()
        conn.close()
        flash('Registration successful, you can now log in.', 'success')
        return jsonify({'username':new_user['username'], 'password':new_user['password']}), 201

@api_routes_bp.route('/api/writeSerial', methods=['POST'])
def api_write_serial():
    from main import write_serial
    print(f"ser in api_write_serial: {ser}")  # nechapu, proc tohle vychazi none, ale ve write serial niz to je inicializovane, kdyz by vsechno melo pouzivat tu samou promennou ser
    #no hlavne ze to funguje
    data = request.get_json()
    timeInSeconds = data['time']
    isMQTT = data['isMQTT']
    write_serial(timeInSeconds, isMQTT)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

@api_routes_bp.route('/api/lastRecord', methods=['GET'])
def get_last_record():
    from main import database
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM last_values")
    last_values = cursor.fetchone()  # fetchone() because we know there's only one row
    conn.close()

    print(f"last_serial_record: {last_values[0]}, last_mqtt_record: {last_values[1]}")
    return jsonify({
        'lastSerialRecord': last_values[0],
        'lastMqttRecord': last_values[1]
})