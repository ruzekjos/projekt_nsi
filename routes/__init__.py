from flask import Blueprint
from .web_routes import web_routes_bp
from .api_routes import api_routes_bp

def register_routes(app):
    app.register_blueprint(web_routes_bp)
    app.register_blueprint(api_routes_bp)