function generateTable(measuredValues) {

    console.log(measuredValues);

    var container = document.getElementById('tableContainer');

    //var rowCount = parseInt(document.getElementById('rowCount').value);

    var count = 0;
    for (var key in measuredValues) {
        if (measuredValues.hasOwnProperty(key)) {
            count++;
        }
    }

    var rowCount = count;

    // Vytvoření tabuky
    var table = document.createElement('table');
    table.border = '1'; // Nastavení rámečků tabulky

    // Vytvoření záhlaví tabulky
    var headerRow = table.insertRow();

    var headerCell1 = headerRow.insertCell(0);
    headerCell1.innerHTML = 'ID';

    var headerCell1 = headerRow.insertCell(1);
    headerCell1.innerHTML = 'Time';

    var headerCell2 = headerRow.insertCell(2);
    headerCell2.innerHTML = 'Temperature';

    headerRow.style.backgroundColor = '#f0f0f0';
    headerRow.style.fontWeight = 'bold';

    // Vytvoření obsahu tabulky
    for (var i = rowCount-1; i >= 0; i--) {
        var row = table.insertRow();
        var cell = row.insertCell();
        cell.innerHTML = measuredValues[i]['id'];
        var cell = row.insertCell();
        cell.innerHTML = measuredValues[i]['timestamp'];
        var cell = row.insertCell();
        cell.innerHTML = Math.round(measuredValues[i]['temperature'] * 10) / 10;

            // Styly pro buňky
        row.style.backgroundColor = i % 2 === 0 ? '#f9f9f9' : '#ffffff';
        row.style.textAlign = 'center';
    }   

    // Vložení tabulky do kontejneru
    container.innerHTML = '';
    container.appendChild(table);
}

document.addEventListener('DOMContentLoaded', function() {
    generateTable(measuredV);
});
